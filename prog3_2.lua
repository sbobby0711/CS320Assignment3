function tokenize(str)
  local ret = {}, i
  for i in string.gmatch(str, "%S+") do
    if string.match(i, "%d+") then
      table.insert(ret, {"num", i})
    else
      table.insert(ret, {"op", i})
    end
  end
  return ret
end

function compareOp(a, b)
  if b == ')' then
    return a ~= '('
  end

  if (a == '*' or a == '/' or a == '+' or a == '-')
  and (b == '+' or b == '-') then
    return true
  end

  if (a == '*' or a == '/') and (b == '*' or b == '/') then
    return true
  end

  return false
  
end

function printStack(s)
  local v = ''
  for k, i in pairs(s) do 
    v = v .. i .. ' '
  end
  print(v)
end

function printStack2(s)
    for k, i in pairs(s) do 
      printStack(i)
    end
    
end
function prepare(str)
  local opstack  = {} 
  local nstack   = {}
  local outstack = {}
  local tokens   = tokenize(str)
  for i = 1, #tokens do
    local token = tokens[i]
    if token[1] == "num" then
       table.insert(outstack, token[2])
    else
       if token[2] == '(' then
         table.insert(opstack, token[2])
       else
            while #opstack > 0 and compareOp(opstack[#opstack], token[2]) do
                local op = table.remove(opstack)
                table.insert(outstack, op)
            end
            if token[2] == ')' then
                table.remove(opstack)
            else
                table.insert(opstack, token[2])
            end
        end
    end
  end
   while #opstack > 0 do
     local op = table.remove(opstack)
     table.insert(outstack, op)
   end
   return outstack
end
function InfixToPostfix(str)
   local outstack = prepare(str)
   local ret = ''
   for k, v in pairs(outstack) do
     if k ~= 1 then
       ret = ret .. ' '
     end
     ret = ret .. v
   end
   return ret
end

function evalstr(str)
  local outstack = prepare(str)
  local result = {}
  for i = 1, #outstack do
    local item = outstack[i]
    if string.match(item, "%d+") then 
      table.insert(result, tonumber(item))
    else
      local rhs = table.remove(result)
      local lhs = table.remove(result)
      local val
      if item == '+' then val = lhs + rhs end
      if item == '-' then val = lhs - rhs end
      if item == '*' then val = lhs * rhs end
      if item == '/' then val = lhs / rhs end
      table.insert(result, val)
    end
  end
  return table.remove(result)
end

if not header then
  print("Assignment #3-2, Weihao Chang, wchang930711@gmail.com")
  local str = io.stdin:read()
  local ret = InfixToPostfix(str)
  print(ret)
  print(string.format("%d", evalstr(str)))


end
Weihao Chang
wchang930711@gmail.com
Prog #3

Program 1 will take a single command line argument. The command line argument will be the name of a lua
file, and it should then execute the Lua file in a lua environment and create in the
C/C++ program

Program 2 :prog3_2.lua In Lua, implement the Infix to Postfix function that went over in class. The function
InfixToPostfix(str) takes a single argument (it will be an input string.

Program 3 :prog3_3.cpp will be written in C/C++. It will be called prog3_3.cpp and can/should be an
extension of prog3_1.cpp The program will create a lua environment, load/run the file specified
by the command line argument.
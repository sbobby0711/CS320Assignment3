extern "C"{
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>

}
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

int main(int argc, char **argv){

    cout << "Assignment #3-3, Weihao Chang, wchang930711@gmail.com" << endl;


    lua_State * L = luaL_newstate();
    luaL_openlibs(L);
    luaL_dofile(L, argv[1]);

    string s;
    getline(cin, s);
    lua_getglobal(L, "InfixToPostfix");
    lua_pushstring(L, s.c_str());
    lua_pcall(L, 1, 1, 0);
    if(luaL_checkstring(L, 1)){
        cout << lua_tostring(L, 1) << endl;
    }else{
        cerr << "Runtime Error" << endl;
    }


    lua_close(L);
    return 0;
}
